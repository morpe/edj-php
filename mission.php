<?php


function get_mission()
{
	global $NOW;
	$today = dt("Y/m/d",$NOW);

	$misAcc = getJdb( "MissionAccepted" );
	$misCom = getJdb( "MissionCompleted" );
	$misAbb = getJdb( "MissionAbandoned" );
	$misAbb = getJdb( "MissionAbandoned" );
	$misFal = getJdb( "MissionFailed" );

	$hu['color'] = "gree";
	$hu['odor'] = "tanfo";
	$hu['id'] = "100";
	$lol['color'] = "red";
	$lol['odor'] = "morto";
	$lol['id'] = "101";

	//$misAcc = keyAsId( $ );


	$misAbbCom = @array_merge( $misAbb, $misCom );

	foreach ($misAcc as $n => $acc) {
		$misAcc[$n]->status = array( "MissionActive" );
	}

	$com = 0;
	$abb = 0;
	$fal = 0;

	foreach ($misAcc as $n => $acc) {
		//$misAcc[$n]->status = array( "Active" );

		if( $misCom != false ){

			foreach( $misCom as $nn => $mis ){
				
				if(  $acc->MissionID == $mis->MissionID ){
	
					$misAcc[$n]->status = array( $mis->event,$mis );
					$com++;
	
				}
	
			}
		}

		if( $misAbb != false ){

			foreach( $misAbb as $nn => $mis ){
				
				if(  $acc->MissionID == $mis->MissionID ){
	
					$misAcc[$n]->status = array( $mis->event,$mis );
					$abb++;
	
				}
	
			}

		}

		if( $misFal != false  ){

			foreach( $misFal as $nn => $mis ){
				
				if(  $acc->MissionID == $mis->MissionID ){
	
					$misAcc[$n]->status = array( $mis->event,$mis );
					$fal++;
	
				}
	
			}
		}

	}

	$huff = array();

	foreach ($misAcc as $n => $acc) {

		if( $acc->status[0] == "MissionActive" ){

			//$huff[] = array( dt("Y M d H:m:s",$NOW), dt("Y M d H:m:s",$acc->Expiry), strtotime( $acc->Expiry ) - strtotime($NOW) ); 

			// $NOW è su edj.php
			$ts = strtotime( $acc->Expiry ) < strtotime($NOW);
			//$ts = strtotime($NOW) > strtotime( $acc->Expiry );
			
			if( $ts ){
				$misAcc[$n]->status = array( "MissionExpired" );
			}
			//var_dump($acc);
		}

	}

	$destination = (object) array();
	$destination->type = false;
	$destination->system = false;
	$destination->port = false;
	$destination->faction = false;
	$active = false;

	foreach ($misAcc as $n => $acc) {

		if( $acc->status[0] == "MissionActive" ){

			$active[] = $acc;

			if( isset( $acc->DestinationSystem ) ){
				
				$destination->system[] = $acc->DestinationSystem;
			}

			if( isset( $acc->DestinationStation ) ){

				$system = ""; 
				if( isset( $acc->DestinationSystem ) ) $system = @spanIt("@ ".$acc->DestinationSystem,"system");
				
				$destination->port[] = $acc->DestinationStation.$system;
			}
			
			if( isset( $acc->Faction ) ){

				$system = ""; 
				if( isset( $acc->DestinationSystem ) ) $system = @spanIt("@ ".$acc->DestinationSystem,"system");

				$destination->faction[] = $acc->Faction.$system;
			}

			if( isset( $acc->Name ) ){

				$destination->type[] = $acc->Name;
			}
		}

	}

	$completed = false;
	foreach( $misCom as $n => $mis ){

		$test = dt( "Y-m-d",$NOW ) == dt( "Y-m-d",$mis->timestamp );

		if( $test ){
			$mis->today = $test;
			$completed[] = $mis;
		}
	}


	if( $destination->type ) @$destination->type = array_count_values( $destination->type );
	if( $destination->system ) @$destination->system = array_count_values( $destination->system );
	if( $destination->port ) @$destination->port = array_count_values( $destination->port );
	if( $destination->faction ) @$destination->faction = array_count_values( $destination->faction );
	

	$obj = (object) array();
	$obj->huff = $huff;
	$obj->ms_acc = count($misAcc);

	$obj->ms_comp = $com;
	$obj->ms_abb = $abb;
	$obj->ms_fail = $fal;
	$obj->ms_active = $obj->ms_acc - ( $com+$abb+$fal );
	$obj->destinations = $destination;
	$obj->active = $active;
	$obj->completed = $completed;


	return $obj;

}


function getEvents($arr,$find){
	$out = array();
	foreach( $arr as $n => $e ){
		if( $e->event == $find ){
			$out[] = $e;
		}
	}

	return $out;
}


function parse_mission($data)
{
	global $NOW;
	$today = dt("Y/m/d",$NOW);

	$out = (object) array();
	$out->today = $today;

	$misAcc = getEvents($data, "MissionAccepted" );
	$misCom = getEvents($data, "MissionCompleted" );
	$misAbb = getEvents($data, "MissionAbandoned" );
	$misFal = getEvents($data, "MissionFailed" );

	$hu['color'] = "gree";
	$hu['odor'] = "tanfo";
	$hu['id'] = "100";
	$lol['color'] = "red";
	$lol['odor'] = "morto";
	$lol['id'] = "101";

	$misAbbCom = array_merge( $misAbb, $misCom );

	foreach ($misAcc as $n => $acc) {
		$misAcc[$n]->status = array( "MissionActive" );
	}

	$com = 0;
	$abb = 0;
	$fal = 0;

	foreach ($misAcc as $n => $acc) {
		//$misAcc[$n]->status = array( "Active" );

		if( $misCom != false ){

			foreach( $misCom as $nn => $mis ){
				
				if(  $acc->MissionID == $mis->MissionID ){
	
					$misAcc[$n]->status = array( $mis->event,$mis );
					$com++;
	
				}
	
			}
		}

		if( $misAbb != false ){

			foreach( $misAbb as $nn => $mis ){
				
				if(  $acc->MissionID == $mis->MissionID ){
	
					$misAcc[$n]->status = array( $mis->event,$mis );
					$abb++;
	
				}
	
			}

		}

		if( $misFal != false  ){

			foreach( $misFal as $nn => $mis ){
				
				if(  $acc->MissionID == $mis->MissionID ){
	
					$misAcc[$n]->status = array( $mis->event,$mis );
					$fal++;
	
				}
	
			}
		}

	}

	$huff = array();

	foreach ($misAcc as $n => $acc) {

		if( $acc->status[0] == "MissionActive" ){

			//$huff[] = array( dt("Y M d H:m:s",$NOW), dt("Y M d H:m:s",$acc->Expiry), strtotime( $acc->Expiry ) - strtotime($NOW) ); 

			// $NOW è su edj.php
			$ts = strtotime( $acc->Expiry ) < strtotime($NOW);
			//$ts = strtotime($NOW) > strtotime( $acc->Expiry );
			
			if( $ts ){
				$misAcc[$n]->status = array( "MissionExpired" );
			}
			//var_dump($acc);
		}

	}

	$destination = (object) array();
	$destination->type = false;
	$destination->system = false;
	$destination->port = false;
	$destination->faction = false;
	$active = false;

	foreach ($misAcc as $n => $acc) {

		if( $acc->status[0] == "MissionActive" ){

			$active[] = $acc;

			if( isset( $acc->DestinationSystem ) ){
				
				$destination->system[] = $acc->DestinationSystem;
			}

			if( isset( $acc->DestinationStation ) ){

				$system = ""; 
				if( isset( $acc->DestinationSystem ) ) $system = @spanIt("@ ".$acc->DestinationSystem,"system");
				
				$destination->port[] = $acc->DestinationStation.$system;
			}
			
			if( isset( $acc->Faction ) ){

				$system = ""; 
				if( isset( $acc->DestinationSystem ) ) $system = @spanIt("@ ".$acc->DestinationSystem,"system");

				$destination->faction[] = $acc->Faction.$system;
			}

			if( isset( $acc->Name ) ){

				$destination->type[] = $acc->Name;
			}
		}

	}

	$completed = false;
	foreach( $misCom as $n => $mis ){

		$test = dt( "Y-m-d",$NOW ) == dt( "Y-m-d",$mis->timestamp );

		if( $test ){
			$mis->today = $test;
			$completed[] = $mis;
		}
	}


	if( $destination->type ) @$destination->type = array_count_values( $destination->type );
	if( $destination->system ) @$destination->system = array_count_values( $destination->system );
	if( $destination->port ) @$destination->port = array_count_values( $destination->port );
	if( $destination->faction ) @$destination->faction = array_count_values( $destination->faction );
	

	$obj = (object) array();
	$obj->fn_name = "parse_mission";
	$obj->ms_acc = count($misAcc);

	$obj->ms_comp = $com;
	$obj->ms_abb = $abb;
	$obj->ms_fail = $fal;
	$obj->ms_active = $obj->ms_acc - ( $com+$abb+$fal );
	$obj->destinations = $destination;
	$obj->active = $active;
	$obj->completed = $completed;


	return $obj;

}


function mission_book($pmis='false'){
	global $NOW;

	$out = $pmis;

	//var_dump($pmis);

	$mis["active"] = $out->active;
	$mis["completed"] = $out->completed;

	$html = array();
	
	foreach( $mis as $type => $obj ){
		$html[] = "<div class='msn $type' >";
		//var_dump($obj);

		if( is_array( $obj ) ){

			foreach( $obj as $lab => $m ){
				$name = str_ireplace("Mission_","",$m->Name);
				$name = str_ireplace("_"," ",$name);

				if( isset( $m->Expiry ) ){
					
					$expiry = "<div class='expiry'>expiry: ".difDate( $m->Expiry,$NOW )."</div>";
				}
				else{
					
					$expiry = "";
				}

				$count ='';
				if( isset($m->Count) ){
					$count = "[$m->Count]";
				}

				$get = "";
				if( isset( $m->Commodity_Localised ) ){
					$get = "get: ".spanIt($m->Commodity_Localised).spanIt(" $count");
				}
				
				
				$port ="";
				if( isset( $m->DestinationStation ) ){
					$port = "port: ".spanIt($m->DestinationStation);
				}
				$target = "";
				if( isset( $m->Target_Localised ) ){
					$target = "target: ".spanIt($m->Target_Localised);
				}

				$system ="";
				if( isset( $m->DestinationSystem ) ){
					$system = "system: ".spanIt($m->DestinationSystem);
				}

				$faction ="";
				if( isset( $m->Faction ) ){
					$faction = "Faction: ".spanIt($m->Faction);
				}
				
				$rep = "";
				if( isset( $m->Reputation ) ){
					$rep = "Reputation: ".spanIt($m->Reputation);
				}
				
				$inf = "";
				if( isset( $m->Influence ) ){
					$inf = "Influence: ".spanIt($m->Influence);
				}

				$rwd = "";
				if( isset( $m->Reward ) ){
					$rwd = "reward: ".spanIt($m->Reward."cr");
				}

				$html[] = "<div id='mis$m->MissionID' class='msn'>";

				$html[] = "<div class='mis-head' >";
				$html[] = "<h2>$name</h2>";
				$html[] = $expiry;
				$html[] = "</div>";
				$html[] = "<div class='clear'><!-- clear --></div>";
				$html[] = "<div class='body'>";
				$html[] = "<div class='ms-prop'>$system</div>";
				$html[] = "<div class='ms-prop'>$port</div>";
				$html[] = "<div class='ms-prop'>$faction</div>";
				$html[] = "<div class='ms-prop'>$target</div>";
				$html[] = "<div class='ms-prop'>$rep</div>";
				$html[] = "<div class='ms-prop'>$inf</div>";
				$html[] = "<div class='ms-prop reward'>$rwd</div>";
				$html[] = "</div>";

				




				$html[] = "</div>";

			}
		}

		$html[] = "</div>";

	}



	$out =  implode("\n",$html);

	return $out;

}

?>
<?php

function lg($str = false)
{
	$date = date("d/m/Y H:i:s");

	if ($str != false) {
		if (is_array($str)) {
			$str = "[array] $date: ".json_encode($str, JSON_PRETTY_PRINT);
		} elseif (is_object($str)) {
			$str = "[object] $date: ".json_encode($str, JSON_PRETTY_PRINT);
		} else {
			$str = "[string] $date: ".json_encode($str);
		}

		$str .= "\n ------------------------------------------------------------------\n\n";

		file_put_contents("edj.log", $str, FILE_USE_INCLUDE_PATH | FILE_APPEND );
	}
}

function spanIt($str='span-it',$cls=''){
	$cls = "class='$cls'";
	return "<span $cls>$str</span>";
}

function dt( $format,$date ){
	
	$ts = strtotime( $date );

	return date( $format,$ts );

}


function difDate($a,$b){
	
		$out = array();
		
		$a = new DateTime($a);
		$b = new DateTime($b);

		$dif=$a->diff($b);
		
		if( $dif->d >= 7 ){
			$dif->w = (int) round( $dif->d / 7 );
			$dif->d = $dif->d - ( $dif->w * 7 );
		}
		else{
			$dif->w = 0;
		}
	
		$arr = explode("-","y-m-w-d-h-i-s");
		foreach( $arr as $n => $l ){
	
			if( $dif->$l != 0 ){
				$out[$l] = $dif->$l.strtoupper($l);
			}
		}
	
		if( $dif->y != 0 or $dif->m != 0 ){
	
			unset( $out["i"] );
			unset( $out["s"] );
		}

		//var_dump( array( $out,$a,$b ) );

		if( !empty( $out ) ){

			$out = implode( " ",$out );
			$out = str_ireplace( "i","M",$out );
			
			return $out;
		}
		else{
			
			return false;
		}
}
	

function dbSync(){
	global $edj,$BigData;

	foreach( $edj->db_dir as $n => $f ){

		if( !stristr($f,"jdb.") ){
			unset( $edj->db_dir[$n]  );
		}

		if( stristr($f,"index") ){
			unset( $edj->db_dir[$n]  );
		}
	}

	$out[0] = count( $edj->db_dir);
	$out[1] = count($edj->index->by_event);
	$out[2] = $out[0] == $out[1];

	return  $out;

}


function remove_files_with($dir, $str = '')
{
	foreach (scandir($dir) as $n => $f) {
		if (stristr($f, $str) and !stristr($f, "index")) {
			$t = unlink($dir.$f);
			//lg( array( "remove_files_with", $dir.$f, $t ) );
		}
	}
}

function setConfig($json)
{
	$obj = (object)array();
	
	if (!isset($json)) {
		$obj->edj = "path to edj";
		$obj->edm = "path to edm";
		
		$json = json_encode($obj, JSON_PRETTY_PRINT);
	} else {
		$json = json_encode($json, JSON_PRETTY_PRINT);
	}
	$err = file_put_contents("config.json", $json, FILE_USE_INCLUDE_PATH);
}

function config()
{
	global $edj, $DB;
	
	$out = (object)array();
	//$err = array();
	
	if (file_exists("config.json")) {
		$out = json_decode(file_get_contents("./config.json", FILE_USE_INCLUDE_PATH));
	
		if (!isset($out)) {
			echo "probably errors on config.json";
			return false;
		}
	} else {
		echo "config.json dont existe!";
		return false;
	}

	return $out;
}


function getJdb($jdb=false){
	global $edj;

	$path = strtolower( $edj->db."jdb.".$jdb.".json" );
	if( file_exists( $path ) ){
		
		$db = file_get_contents( $path );
		$db = json_decode( $db );
	
		$db = array_reverse( $db );
		//var_dump($db);
		return $db;

	}else{
		return false;
	}
}

function if_dir_sync()
{
	global $edj;

	$sd_tmp = scandir($edj->tmp->journal);
	$tmp_c = 0;
	foreach ($sd_tmp as $n => $file) {
		if (!stristr($file, "journal.")) {
			unset( $sd_tmp[$n] );
		}
	}
	
	$sd_edj = scandir($edj->config->edj);
	$edj_c = 0;
	foreach ($sd_edj as $n => $file) {
		if (!stristr($file, "journal.")) {
			unset( $sd_edj[$n] );
		}
	}
	
	$dif1 = array_diff($sd_edj,$sd_tmp);
	$dif2 = array_diff($sd_tmp,$sd_edj);


	//var_dump( array( $dif1, $dif2, $dif1 == $dif2 ) );

	return $dif1 == $dif2;
}

//Journal log to array
function getBigData()
{
	global $edj;

	if( $edj->init == false ) return false;

	$BD = array();
	foreach (scandir( $edj->tmp->journal ) as $n => $file) {
		//var_dump( $n, $file );

		if (stristr($file, "Journal.")) {
			$arr = file( $edj->tmp->journal.$file );
			
			foreach ($arr as $nn => $json) {
				//fix :inf - nel json
				$json = str_ireplace(":inf", ":" . "inf", $json);

				$obj = json_decode($json);

				if (json_last_error() != 0) {
					//lg("json error! $file");
				} else {
					array_push($BD, $obj);
				}
			}
		}
	}

	return $BD;
}


//genera lindice del db
function dbIndex(){
	global $edj,$BigData;

	if( $edj->init == false ){
		return false;
	}

	$arr = array();
	foreach( $BigData as $n => $obj ){
		$e = $obj->event;
		array_push($arr,$e);
	}

	$out = (object) array();
	
	$out->by_event = (array) array_unique($arr);
	sort( $out->by_event );


	$out->for_list = array_count_values($arr);

	$out->log_list = array();
	$out->log_md5 = array();

	foreach( scandir( $edj->config->edj ) as $n => $f ){
		
		if( stristr( $f,"journal." ) ){
			array_push( $out->log_list,$f );

			//$file = file_get_contents($edj->config->edj->$f);
			$file = file_get_contents( $edj->config->edj.$f );


			array_push( $out->log_md5, array($f=>md5( $file ) )  );
		}
		
	}

	file_put_contents( $edj->db."jdb.index.json", json_encode($out) );

	return $out;

}

function init()
{
	global $edj;

	

	$out = false;

	if (!isset($edj->config)) {
		//lg("config unset");
		return false;
	}

	if (if_dir_sync()) {

		$out = true;

		//var_dump("OK!");
		//lg( "journal and tmp synced" );
	} else {
		remove_files_with($edj->tmp->journal, ".log");
		//remove_files_with($edj->db, "jdb.");

		$scan = scandir($edj->config->edj);
		foreach ($scan as $n => $file) {
			if (stristr($file, "Journal.")) {
				if (!file_exists($edj->tmp->journal . $file)) {
					if (mime_content_type($edj->config->edj . "/$file") == "text/plain") {
						$copy = copy($edj->config->edj . "/$file", "./tmp/journal/$file");
						//lg( array( "copy: $file",$copy ) );
					} else {
						$err = (object) array();
						$err->timestamp = date("d/m/Y H:i:s");
						$err->event = "errror";
						$err->mime = mime_content_type($edj->config->edj . "/$file");
						$err->info = "contain invalid or uncknow data";
						$err->file = $file;

						file_put_contents( $edj->config->edj . "/$file", json_encode($err) );
						lg( $err );	
					}
				} else {
					//var_dump("dho!");
				}
			}
		}
		//db_by_event();
	}

	return $out;
}

function thisDay(){
	global $edj, $BigData;

	$thisDay = $edj->thisDay;

	$ev = array();

	foreach( $BigData as $n => $obj ){

		$thisDate = date("d M Y", strtotime( $obj->timestamp ));
		$tmp = date( "d/m/Y H:i:s", strtotime( $obj->timestamp ) );

		if( $thisDay == $thisDate ){

			array_push( $ev, $obj );
		}
	}

	return $ev;
}

function thisWeek(){
	global $edj, $BigData;

	$thisWeek = $edj->thisWeek;

	$ev = array();

	foreach( $BigData as $n => $obj ){

		$thisDate = date("W Y", strtotime( $obj->timestamp ));
		$tmp = date( "d/m/Y H:i:s", strtotime( $obj->timestamp ) );

		if( $thisWeek == $thisDate ){

			array_push( $ev, $obj );
		}
	}

	return $ev;
}


function theme()
{
	global $edj,$BigData;
	$html = file_get_contents($edj->theme->home);
	
	//head
	$head = head();
	$html = str_ireplace("{head}", $head, $html);

	$html = str_ireplace("{script}", ajax(), $html);
	
	//header date
	$html = str_ireplace("{local-time}", $edj->now, $html);

	//stardate
	$html = str_ireplace("{stardate}", starDate(), $html);

	//copmmander ship
	$html = str_ireplace("{Cmdr}", commander(), $html);
	$html = str_ireplace("{Ship}", " of ".ship(), $html);

	// port
	$html = str_ireplace("{port}", location(), $html);

	// missions

	$mis = mission();
	$html = str_ireplace("{mission-summary}", $mis->summary, $html);

	$pmis = parse_mission( $BigData );
	$html = str_ireplace("{ms-acc}", $pmis->ms_acc, $html);
	$html = str_ireplace("{ms-comp}", $pmis->ms_comp, $html);
	$html = str_ireplace("{ms-abb}", $pmis->ms_abb, $html);
	$html = str_ireplace("{ms-fail}", $pmis->ms_fail, $html);
	
	$active_mission = mission_book($pmis);
	$html = str_ireplace("{mission-active}", $active_mission, $html);

	$completed_mission = mission_book($pmis);
	$html = str_ireplace("{mission-completed}", $completed_mission, $html);
	
	//event list
	$html = str_ireplace("{event-list}", eventList(), $html);
	
	//table
	if (!isset($edj->get->table)) {
		$html = str_ireplace("{event-table}", "<!-- table -->", $html);
	} else {
		$html = str_ireplace("{event-table}", eventTable(), $html);
	}

	echo $html;
}

function doc()
{
	global $edj;

	if( $edj->init == false ){
		echo "<script>setTimeout( function(){ location.replace(\"./\") },1000*30 );</script>";
		echo "<h2>Copyng journal log...</h2>";
		echo "<p>reload in 30 seconds</p>";

	}
	else{
		if (isset( $edj->get->e )) {
			header('Content-Type: application/json; charset=utf-8');
		} else {
			header('Content-Type: text/html; charset=utf-8');
			theme();
		}
	}
}

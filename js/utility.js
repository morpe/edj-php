
function hu (str){
	return str;
}


function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

function span(str) {
	return "<span>" + str + "</span>";
}

function isset(obj) {
	if (obj) {
		return obj;
	}
	else {
		return false;
	}
}

function memorySizeOf(obj) {
	var bytes = 0;

	function sizeOf(obj) {
		if (obj !== null && obj !== undefined) {
			switch (typeof obj) {
				case 'number':
					bytes += 8;
					break;
				case 'string':
					bytes += obj.length * 2;
					break;
				case 'boolean':
					bytes += 4;
					break;
				case 'object':
					var objClass = Object.prototype.toString.call(obj).slice(8, -1);
					if (objClass === 'Object' || objClass === 'Array') {
						for (var key in obj) {
							if (!obj.hasOwnProperty(key)) continue;
							sizeOf(obj[key]);
						}
					} else bytes += obj.toString().length * 2;
					break;
			}
		}
		return bytes;
	};

	function formatByteSize(bytes) {
		if (bytes < 1024) return bytes + " bytes";
		else if (bytes < 1048576) return (bytes / 1024).toFixed(3) + " KiB";
		else if (bytes < 1073741824) return (bytes / 1048576).toFixed(3) + " MiB";
		else return (bytes / 1073741824).toFixed(3) + " GiB";
	};

	//return formatByteSize(sizeOf(obj));
	return { "bytes": sizeOf(obj), "human": formatByteSize(sizeOf(obj)) };
};

var re = function () {
	$('.reload').replaceWith("<link class='reload' rel='stylesheet' href='./theme.css?r=" + Sugar.Number.random(0, 9999) + "'>");

};

function relativeTime(time) {
	var t = Math.round(time);

	obj = {};
	obj.t = t;

	var dl = 24 * 60 * 60;
	var hl = 60 * 60

	console.log(time % dl);

	obj.hour = false;
	obj.minutes = false;
	obj.seconds = false;

	return obj;
}

function obj2html(obj, str) {

	//determinare tipo di oggetti
	here = {};

	here.arr = [];
	here.list = [];
	here.simpleObj = [];
	here.complexObj = [];
	here.type = false;
	here.length = false;
	here.input = obj;
	here.keys = [];
	here.range = [0, 500];

	if (str) here.title = str;
	else here.title = "obj2html";

	here.html = "";

	here.Is = function (obj) {

		$.each(obj, (a, b) => {

			if (typeof b == "number") {
				here.list.push(b);
				here.type = "list";
				here.List();
			}
			else if (typeof b == "string") {
				here.list.push(b);
				here.type = "list";
				here.List();
			}
			else if (typeof b == "bolean") {
				here.list.push(b);
				here.type = "list";
				here.List();
			}
			else if (typeof b == "object") {

				here.type = "object";

				if (b[0]) {
					here.arr.push(b);
					here.is = "array";
					here.lenght = b.length;
				}
				else {

					if (Object.keys(b).length < 3) {
						here.simpleObj.push(b);
						here.is = "tag";
						here.Tag();

					}
					else {
						here.complexObj.push(b);
						here.is = "table";
						here.Table();
					}
				}

			}
		});

		return here;
	}

	here.List = function () {
		var ul = "<ul class='list-" + here.title + "'>";
		$.each(here.list, (n, li) => {
			ul += "<li>" + li + "</li>";
		});
		ul += "</ul>";
		here.html = ul;
	}

	here.Tag = function () {
		$.each(here.simpleObj, (n, item) => {
			Sugar.Object.merge(here.keys, Object.keys(item));
		});

		var tag = "";

		$.each(here.simpleObj, (n, item) => {

			var a = here.keys[0];
			var b = here.keys[1];

			tag += "<div id='tag" + n + "' class='tag-" + here.title + "'>";
			tag += "<span class='tag-" + a + "'>" + item[a] + "</span>";
			tag += "<span class='tag-" + b + "'>" + item[b] + "</span>";
			tag += "</div>";
		});

		here.html = tag;
	}

	here.Table = function () {

		var table = "<table id='tbl" + str + "' class='table-" + here.title + "'>";

		$.each(here.complexObj, (n, item) => {
			Sugar.Object.merge(here.keys, Object.keys(item));
		});


		here.tableIndex = [];
		table += "<thead>";
		$.each(here.keys, (n, th) => {
			here.tableIndex[th] = n;
			table += "<th class='th-" + th + " th-" + n + "' data-index='" + n + "'>";
			table += th;
			table += "</th>";
		});
		table += "</thead>";

		table += "<tbody>";

		var nn = Sugar.Object.find(journal.size, function (f) {
			return f.name == str;
		});

		size = journal.size[nn].bytes;

		$.each(here.complexObj, (n, tr) => {

			table += "<tr>";
			$.each(here.keys, (nn, key) => {

				var id = "r" + n + "c" + nn;
				var col = "col-" + key;

				//console.log(nn,n,key);

				if (tr[key]) {
					if (typeof tr[key] == "object") {

						table += "<td id='" + id + "' class='json " + col + "'>";
						table += "<span class='icon'><!-- obj --></span>";
						table += "<span data-target='#" + id + "' data-title='" + key + "' class='data hide'>" + JSON.stringify(tr[key]) + "</span>";
						table += "</td>";

					}
					else {

						table += "<td id='" + id + "' class='" + col + "'>";
						table += tr[key];
						table += "</td>";

					}


				}
				else {
					table += "<td id='" + id + "' class='" + col + "'>";
					table += "<!-- empty -->";
					table += "</td>";

				}

			})
			table += "</tr>";
		});

		table += "</tbody>";

		table += "<tfoot>";
		$.each(here.keys, (n, th) => {
			table += "<th>";
			table += th;
			table += "</th>";
		});
		table += "</tfoot>";

		table += "</table>";

		var h2 = "<h2>EVENT: " + Sugar.String.titleize(str) + "</h2>";
		here.html = h2 + table;
		here.tableId = "#tbl" + str;

	}

	//journal.tmp = { obj2html: here };
	return here.Is(obj);

}

function recall() {
	$("td.json span.data").each((a, b) => {
		var id = $(b).data().target;
		var title = $(b).data().title;
		var data = obj2html(JSON.parse($(b).html()), title);
		//console.log(id,data,data.html);
		$(id).html(data.html);

	});
}

function formatNumber(n) {
	var n = new Intl.NumberFormat().format(n);
	return n;
}

function edTime2Ts() {
	$("td.col-Expiry").each(function () {
		var tmp = new Date($(this).text());
		console.log("edTime2Ts", this, tmp, tmp.getTime());
		ts = tmp.getTime();
		$(this).data({ "ts": ts, "date": tmp });
		$(this).html(ts);

	});
}

function ts2Date() {

	$("td.col-timestamp,td.col-Expiry").each(function () {
		var obj = new Date(parseInt($(this).text()));
		var D = obj.getDate();
		var M = obj.getMonth() + 1;
		var Y = obj.getFullYear();

		var H = obj.getHours();
		var P = obj.getMinutes();
		var S = obj.getSeconds();

		$(this).html(D + "/" + M + "/" + Y + " " + H + ":" + P + ":" + S);

	});
}

function simpleDate(x){
	var date = new Date(x);
	return Sugar.Date.format( date ,"{dd} {Month} {year} {HH}:{mm}:{ss}");
}

function replaceAll(tg,find,str){

	return Sugar.String.replaceAll(tg,find,str);
}


if( typeof module == "undefined"  ){
	module = {};
}


module.exports = { 
	obj2htmloff: function(obj,str){
		return obj2html(obj, str);
	},
	hu:function(str){
		return str+" HUUUU!!!";
	}
}
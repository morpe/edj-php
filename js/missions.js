msn = {}
msn.actHtml = "";
msn.val = {"Low":"+","Med":"++","High":"+++"};
msn.tab = function(ts){
	console.log(ts);

	if(ts == 0){
		$("div.msn").addClass("show");
		$("td.tab").removeClass("on");
		$("td.all").addClass("on");
	}

	if(ts == 1){
		$("div.msn").removeClass("show");
		$("div.msn.Active").addClass("show");

		$("td.tab").removeClass("on");
		$("td.active").addClass("on");
	}

	if(ts == 2){
		$("div.msn").removeClass("show");
		$("div.msn.Completed").addClass("show");

		$("td.tab").removeClass("on");
		$("td.completed").addClass("on");
	}



}
msn.Active = function(){
	var act = [];
	var now = XDate();
	$.each(msn.data.MissionAccepted,function(n,obj){

		var xp = XDate( obj.Expiry );
		var diff = now.getTime() > xp.getTime();
		var html = "";

		obj.status = "Active";
		obj.reward = false;
		obj.rewComodity = false;
		obj.rewCount = false;

		Sugar.Array.filter(msn.data.MissionCompleted,function(aa){
			if( aa.MissionID == obj.MissionID ){
				obj.status = "Completed";
				obj.reward = aa.Reward;
				obj.rewComodity = aa.CommodityReward;
				obj.rewCount = aa.CommodityReward;
				//console.log(aa);
			}
		});
		Sugar.Array.filter(msn.data.MissionFailed,function(aa){
			if( aa.MissionID == obj.MissionID ){
				obj.status = "Failed";
			}
		});
		Sugar.Array.filter(msn.data.MissionAbandoned,function(aa){
			if( aa.MissionID == obj.MissionID ){
				obj.status = "Abandoned";
			}
		});

		if( !diff ){

			//console.log(obj);

			var D = Sugar.Date.range( Date(),obj.Expiry ).days();
			var H = Sugar.Date.range( Date(),obj.Expiry ).hours();
			var M = Sugar.Date.range( Date(),obj.Expiry ).minutes();

			if( H > 23 ){
				var tmp = H - (24 * D);
				H = tmp;
			}

			if( M > 59 ){
				var tmp1 = 60 * 24 * D;
				var tmp2 = 60 * H;
				M = M - (tmp1 + tmp2);
			}

			var title = obj.Name;
			var status = obj.status;
			var expiry = D+"D "+H+"H "+M+"M";
			var rep = msn.val[obj.Reputation];
			var inf = msn.val[obj.Influence];
			var reward = obj.reward;

			if( obj.Commodity_Localised ){

				var get = { item:obj.Commodity_Localised, count:obj.Count }
			}
			else{
				var get = { item:"", count:"" }

			}


			var toSystem = obj.DestinationSystem;
			var toStation = obj.DestinationStation;
			var faction = obj.Faction;

			var show ='';
			if( status == "Active" ){
				var show ='show';
			}

			html += "<div class='msn "+status+"'>";
			html += "<div class='head'>";
			html += "<span class='status'>"+status+"</span>";
			html += "<span class='time'>"+expiry+"</span>";

			html += "</div>";
			html += "<div class='type'>type: "+span(title)+"</div>";
			html += "<div class='to'>to: <span>"+toSystem+"</span> at <span>"+toStation+"</span></div>";
			html += "<div class='for'>For: <span>"+faction+"</span></div>";
			
			if( obj.Commodity_Localised ){
				html+= "<div class='cargo'>cargo: <span> "+obj.Count+" "+obj.Commodity_Localised+" </span></div>";
			}
			
			html += "<div class='rep-inf'><span>Rep"+rep+"</span> <span>Inf"+inf+"</span></div>";

			if( reward ){
				var rewComodity = "";
				if(obj.rewComodity ){
					rewComodity = "comodity: "+obj.rewCount+" "+obj.rewComodity;
				}
				html += "<div class='reward'>Reward: <span>"+formatNumber(reward)+" cr</span>  <span>"+rewComodity+"</span></div>";
			}


			html += "</div>"; 

			msn.actHtml += html;

			act.push(obj);
			
		}
	});

	msn.act = act;
	//console.log(act);
}

msn.Run = function(){
	msAcc = journal.data.MissionAccepted;
	msComp = journal.data.MissionCompleted;
	msAbb = journal.data.MissionAbandoned;
	msFail = journal.data.MissionFailed;
	msALL = [];

	msn.data = {
		"MissionAccepted":msAcc,
		"MissionCompleted":msComp,
		"MissionAbandoned":msAbb,
		"MissionFailed":msFail,
		"delta": msAcc.length - msAbb.length ,
		"all":msALL
	}
}

function Msn(){
	//console.log("msn",msn);
	msn.Run();
	msn.Active();
}
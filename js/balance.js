bal = {};
bal.data = [];
bal.now = new XDate();
bal.Deltas = function () {
	var lastDay = []
	var lastWeek = []
	var lastMonth = []

	$.each(bal.data, function (a, b) {

		var date = new XDate(b.date);
		var diff = bal.now.diffDays(date);
		diff = Math.ceil(Math.abs(diff));

		if (diff < 3) {
			lastDay.push(b.cr);
			//console.log(diff, b.date);
		}

		if (diff < 8) {
			lastWeek.push(b.cr);
			//console.log(diff, b.date);
		}

		if (diff < 31) {
			lastMonth.push(b.cr);
			//console.log(diff, b.date);
		}
	});

	jQuery.unique(lastDay);
	//lastDay.sort(function (a, b) { return a - b });

	jQuery.unique(lastWeek);
	//lastWeek.sort(function (a, b) { return a - b });

	jQuery.unique(lastMonth);
	//lastMonth.sort(function (a, b) { return a - b });

	bal.yersterday = {
		"lastDay":lastDay,
		"cr": _.first(lastDay),
		"dif": _.last(lastDay) - _.first(lastDay)
	};

	bal.thisWeek = {
		"lastWeek":lastWeek,
		"cr": _.first(lastWeek),
		"dif": _.last(lastWeek) - _.first(lastWeek)
	};

	bal.thisMonth = {
		"lastMonth":lastMonth,
		"cr": _.first(lastMonth),
		"dif": _.last(lastMonth) - _.first(lastMonth)
	};


	bal.today = _.last(lastDay);

};
bal.Make = function () {

	$.each( journal.data.LoadGame, function (a, b) {

		var BAL = {};
		BAL.date = b.timestamp;
		BAL.ts = new XDate(b.timestamp).getTime();
		BAL.cr = b.Credits;
		bal.data.push(BAL);


	});

	bal.data = _.sortBy(bal.data, "timestamp");

}

function Bal() {
	bal.Make();
	bal.Deltas();
	console.log(bal);
}


/*
find:
TotalReward: date,cr, altro
Reward: data,cr, altro
Cost: data,cr,cosa?
TotalCost: date,cr,cosa?
Bounty: date, cr, crime
Fine: date, cr, crime
TransferCost: date,cr,ship,cosa?
TransferPrice
BuyPrice: date
SellPrice
TotalSale
Ammount
BaseValue
ShipPrice

*/
function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

function relativeTime(time) {
	var t = Math.round(time);


	obj = {};
	obj.t = t;

	var dl = 24 * 60 * 60;
	var hl = 60 * 60

	console.log(time % dl);

	obj.hour = false;
	obj.minutes = false;
	obj.seconds = false;



	return obj;


}

function formatNumber(n) {
	var n = new Intl.NumberFormat().format(n);

	return n;
}

function strIreplace(find, set, tg) {

	var test = typeof find == 'string' && typeof set == "string" && typeof tg == "string";

	var count;
	var pos;

	if (test) {

		while (pos > -1) {
			++count;
			pos = tg.indexOf(find, ++pos);
		}

		console.log("HUU!!", count);
		return tg;
	}
	else {
		console.log("doh!!");
		return tg;
	}

}


function edTime2Ts() {
	$("td.col-Expiry").each(function () {
		var tmp = new Date($(this).text());
		console.log("edTime2Ts", this, tmp, tmp.getTime());
		ts = tmp.getTime();
		$(this).data({ "ts": ts, "date": tmp });
		$(this).html(ts);

	});
}

function ts2Date() {

	$("td.col-timestamp,td.col-Expiry").each(function () {
		var obj = new Date(parseInt($(this).text()));

		//console.log( $(this).attr("class"),  obj );

		var D = obj.getDate();
		var M = obj.getMonth() + 1;
		var Y = obj.getFullYear();

		var H = obj.getHours();
		var P = obj.getMinutes();
		var S = obj.getSeconds();

		$(this).html(D + "/" + M + "/" + Y + " " + H + ":" + P + ":" + S);

	});
}

function Now() {
	var obj = new Date();
	var D = obj.getDate();
	var M = obj.getMonth() + 1;
	var Y = obj.getFullYear();

	var H = obj.getHours();
	var P = obj.getMinutes();
	var S = obj.getSeconds();

	//$(this).html(Y + "/" + M + "/" + D + "<br>" + H + ":" + P + ":" + S);
	//now.getSeconds();
	$("#now").html(D + "/" + M + "/" + Y + " " + H + ":" + P + ":" + S);

}

function spanWithJson() {
	$("span.json-obj").each(function () {


		var type = $(this).data('type');
		var obj = JSON.parse($(this).text());
		//$(span).text("");

		var span = "";
		$.each(obj, function (n, item) {

			if (type == "Inventory" ||
				type == "Manufactured" ||
				type == "Raw" ||
				type == "Ingredients" ||
				type == "CommodityReward" ||
				type == "Encoded"
			) {
				if (item.Name && item.Count) {
					span += "<span class='itmNameCount'>" + item.Name + "(" + item.Count + ")</span>";
				}
			}
			else if (type == "Rewards" ||
				type == "Killers" ||
				type == "Modules" ||
				type == "Items" ||
				type == "Manifest" ||
				type == "Rings" ||
				type == "Factions"
			) {
				$.each(item, function (label, str) {
					span += "<span class='label'>" + label + ": </span>";
					span += "<span class='str'>" + str + "</span><br>";
				});
				span += "<br>";
			}
			else if (type == "StarPos") {
				span += item + "<br>";
			}
			else if (type == "AtmosphereComposition" || type == "Materials") {
				span += "<span class='itmNameX100'>" + item.Name + " " + item.Percent + "%</span>";
			}
			else if (type = "System") {
				span += "<span class='itmNameStr'>" + item + "</span>";
			}
			else {
				span += JSON.stringify(item) + "<br>";
				span += "is " + typeof item + "<br>";
				span += "type " + type + "<br><br>";
			}

		});

		$(this).html(span);
	});
}

function arr2table(ar, head, title, type) {

	var row = [];

	$.each(ar, function (n, obj) {
		row[n] = [];
		$.each(head, function (nn, hd) {

			if (obj[hd]) {
				row[n].push({ "label": hd, "str": obj[hd] });
			}
			else {
				row[n].push({ "label": hd, "str": "" });
			}
		});
	});

	var html = "<div class='table " + type + "'>";
	html += "<div class='title'>" + title + "</div>";
	html += "<table class='" + type + "'>";
	html += "<thead>";
	html += "<tr class='head'>";
	$.each(head, function (n, str) {
		//html +="<th data-sort='"+str.replace("_Localised","")+"'>"+str.replace("_Localised","")+"</th>";
		html += "<th data-sort='" + str + "' class='col" + n + " col-" + str + "'>" + str + "</th>";
	});
	html += "</tr>";
	html += "</thead>";

	html += "<tbody>";
	var cc = 0;
	$.each(row, function (n, rw) {

		var oddevene = "odd";
		if (cc % 2 == 0) {
			var oddevene = "even";
		}

		html += "<tr data-ts='" + ar[n].timestamp + "' class='entry " + oddevene + "'>";
		$.each(rw, function (l, obj) {

			if (typeof obj.str == "object" && obj.str.length != 0) {
				obj.json = JSON.stringify(obj.str);
				obj.str = "<span class='json-obj' data-type='" + obj.label + "' >" + obj.json + "</span>";
			}

			html += "<td id='cell" + n + "-" + l + "' class='col-" + obj.label + "' title='" + obj.label + "'>" + obj.str + "</td>";

		});
		html += "</tr>";
		cc++;
	});
	html += "</tbody>";

	html += "</table>";
	html += "</div>";

	return html;

}


function displayEvents(e) {
	var head = [];
	var evs = [];

	journal.data.forEach(function (obj, i) {
		if (obj.event == e) {
			evs.push(obj);
			$.each(obj, function (a, b) {
				head.push(a);
			});
		}
	});

	var head = jQuery.unique(head);

	var tbl = arr2table(evs, head, "Events: " + e, e + " tblSort");
	journal.selectedEvent = evs;
	return { "data": evs, "table": tbl };
}


function evList() {
	var html = "<ul>";

	var ls = journal.events.sort();

	$.each(ls, function (a, b) {
		if (b.length != 0) {
			html += "<li id='evl" + a + "' class='evls' data-ev='" + b + "' onclick='dspLst(this)'>" + b + "</li>";
		}
	});
	html += "</ul>";

	$("#listev div.ls").html(html);
}

function dspLst(ts) {

	Now();

	var dspLst = displayEvents($(ts).data("ev"));
	$(".evls").removeClass("on");
	$(ts).addClass("on");
	$("#dspLst").html(dspLst.table);
	edTime2Ts();
	ts2Date();
	spanWithJson();

	//  implementare: http://js-grid.com/demos/

}

function allEventsByDate() {
	var time = XDate();

	var out = {};
	var last = [];


	$.each(journal.events, function (n, event) {
		out[event] = [];

		$.each(journal.data, function (nn, dt) {

			if (dt.event == event) {

				out[event].push(dt);

			}

		});

	});

	$.each(out, function (n, evn) {
		var tmp0 = _.sortBy(evn, "timestamp");
		var tmp1 = _.last(tmp0);
		last.push(tmp1);
	});

	journal.last = last;

}

//uso in replace all
function span(str) {
	return "<span>" + str + "</span>";
}

function isset(obj) {
	if (obj) {
		return obj;
	}
	else {
		return false;
	}
}

cover = {};
cover.status = {};
cover.Get = function (label) {
	//console.log(label);
	var key = _.findKey(cover.shipSatus, label);
	if (key) {
		return cover.shipSatus[key][label];
	}
	else {
		return false;

	}
}
cover.getShipStatus = function () {

	shipSatus = [];
	shipSatus.push(_.findWhere(journal.last, { "event": "Docked" }));
	shipSatus.push(_.findWhere(journal.last, { "event": "Touchdown" }));
	shipSatus.push(_.findWhere(journal.last, { "event": "Undocked" }));
	shipSatus.push(_.findWhere(journal.last, { "event": "Liftoff" }));
	shipSatus.push(_.findWhere(journal.last, { "event": "Location" }));
	shipSatus.push(_.findWhere(journal.last, { "event": "LoadGame" }));

	shipSatus.sort(function (a, b) {
		//console.log( a.DATE,b.DATE );
		// Turn your strings into dates, and then subtract them
		// to get a value that is either negative, positive, or zero.
		return new Date(b.DATE) - new Date(a.DATE);
	});

	//console.log( shipSatus );

	cover.shipSatus = shipSatus;
}
cover.Html = function () {
	var data = cover.status;
	var html = $("#cover").html();

	html = v.replaceAll(html, "{cmdr}", data.commander);

	if (data.shipName) {
		var ship = "of " + data.shipName + " [" + data.shipType + "]";
		html = v.replaceAll(html, "{ship}", ship);
	}

	/* cover */
	html = v.replaceAll(html, "{stardate}", data.starDate);

	html = v.replaceAll(html, "{bal-today}", formatNumber(bal.today));
	html = v.replaceAll(html, "{bal-yesterday-dif}", formatNumber(bal.yersterday.dif));
	html = v.replaceAll(html, "{bal-thisweek-dif}", formatNumber(bal.thisWeek.dif));
	html = v.replaceAll(html, "{bal-thismonth-dif}", formatNumber(bal.thisMonth.dif));

	html = v.replaceAll(html, "{port}", span(data.port));
	html = v.replaceAll(html, "{dock}", span(v.padLeft(data.dock.LandingPad, 2, "0")));
	html = v.replaceAll(html, "{port-date}", span(data.dockTime));

	html = v.replaceAll(html, "{system}", span(data.system));
	html = v.replaceAll(html, "{alliance}", span(data.alliance));
	html = v.replaceAll(html, "{security}", span(data.security));

	html = v.replaceAll(html, "{body}", span(data.body));
	html = v.replaceAll(html, "{long}", span(data.position.Touchdown.Longitude));
	html = v.replaceAll(html, "{lat}", span(data.position.Touchdown.Latitude));

	html = v.replaceAll(html, "{pos-date}", span(data.touchTime));

	/* -- misison -- */
	html = v.replaceAll(html, "{ms-acc}", span(msn.data.MissionAccepted.length));
	html = v.replaceAll(html, "{ms-comp}", span(msn.data.MissionCompleted.length));
	html = v.replaceAll(html, "{ms-delta}", span(msn.data.MissionAccepted.length - msn.data.MissionCompleted.length));
	html = v.replaceAll(html, "{ms-fail}", span(msn.data.MissionFailed.length));
	html = v.replaceAll(html, "{ms-abb}", span(msn.data.MissionAbandoned.length));
	html = v.replaceAll(html, "{ms-active-list}", msn.actHtml);


	$("#cover").html(html);
}
cover.Run = function () {
	cover.getShipStatus();

	var touch = _.findWhere(journal.last, { "event": "Touchdown" });
	var liftoff = _.findWhere(journal.last, { "event": "Liftoff" });

	cover.status.lastEvent = cover.shipSatus[0].event;

	if (cover.status.lastEvent == "Docked") {
		$("#cover div.port").removeClass("hide");
	}
	else {
		$("#cover div.position").removeClass("hide");
	}


	var stardate = new XDate(cover.shipSatus[0].DATE);
	cover.status.starDate = stardate.toString("MMM d, yyyy");

	cover.status.commander = cover.Get("Commander");
	cover.status.shipName = cover.Get("ShipName");
	cover.status.shipType = cover.Get("Ship");
	cover.status.docked = cover.Get("Docked");
	cover.status.credits = cover.Get("Credits");
	cover.status.bodyType = cover.Get("BodyType");
	cover.status.body = cover.Get("Body");

	cover.status.position = {
		"Touchdown": isset(touch),
		"Liftoff": isset(liftoff)
	}

	cover.status.system = cover.Get("StarSystem");
	cover.status.port = cover.Get("StationName");

	cover.status.dock = _.findWhere(journal.last, { "event": "DockingGranted" });
	var docktime = new XDate(cover.status.dock.DATE);
	cover.status.dockTime = docktime.toString("dd/MM/yy HH:mm:ss");

	if (isset(cover.status.position.Touchdown.DATE)) {
		var touchtime = new XDate(cover.status.position.Touchdown.DATE);
	}
	else {
		cover.status.touchTime = false;

	}

	cover.status.alliance = cover.Get("SystemAllegiance");
	cover.status.security = cover.Get("SystemSecurity_Localised");

	cover.Html();
	$("#cover").delay(250).fadeIn(250);
}

var journal = {}
journal.v = "1.0.0";


function ED() {
	var alldata = [];
	var allEvents = [];

	$.each(ed.edJData, function (line, obj) {


		//console.log(obj);
		ed.edJData[line].DATE = obj.timestamp;
		ed.edJData[line].timestamp = Date.parse(obj.timestamp);

		allEvents.push(obj.event);


		//edJson.DATE = edJson.timestamp;
		//edJson.timestamp = 


		//var edJson = JSON.parse(obj.data);
		//edJson.FILE = obj.file;

		//console.log('475',edJson);


		/*if (IsJsonString(obj.data)) {

			 console.log('479',obj);
			 
			
			
			


			allEvents.push(edJson.event);
			alldata.push(edJson);


		}
		else {
			console.log(line, ed.edj + "/" + obj.file, false);
		}*/


	});

	ed.edJData = Sugar.Array.sortBy(ed.edJData, "DATE");

	journal.data = ed.edJData;
	journal.events = jQuery.unique(allEvents);
}

$(document).ready(function () {

	$.ajax({
		url: "db.json",
		success: function (s) {
			//console.log(s);
			ed = s;
		},
		error: function (e) {
			console.log("err load db.json", e);
			_err_ = e;
		},
		complete: function (c) {
			/* qui parte avvia tutto! */

			ED();
			Now(); //gener a l'ora locale in alto

			Bal(); // run balance.js
			Msn(); // run mission,js

			//allEventsByDate();
			evList();
			//edTime2Ts();
			//ts2Date();
			
			cover.Run();

			journal.fileLoaded = ed.edJFileList.length;

			//console.log("read "+ed.edJFileList.length+" journal logs");
			//console.log("complete",c,journal);
		}
	});

	/*Now();
	
	var alldata = [];
	var allEvents = [];

	$.each(ed.edJData, function (line, obj) {

		if (IsJsonString(obj.data)) {

			var edJson = JSON.parse(obj.data);
			edJson.FILE = obj.file;
			edJson.DATE = edJson.timestamp
			edJson.timestamp = Date.parse(edJson.timestamp);

			allEvents.push(edJson.event);
			alldata.push(edJson);

		}
		else {
			//console.log(line, ed.edj + "/" + obj.file, false);
		}
	});

	journal.data = alldata;
	journal.events = jQuery.unique(allEvents);

	//console.log("functions.js");
	
	Bal();
	Msn();

	allEventsByDate();
	evList();
	edTime2Ts();
	ts2Date();
	cover.Run();
	
	*/

})

function Now() {
	var obj = new Date();
	var D = obj.getDate();
	var M = obj.getMonth() + 1;
	var Y = obj.getFullYear();

	var H = obj.getHours();
	var P = obj.getMinutes();
	var S = obj.getSeconds();

	//$(this).html(Y + "/" + M + "/" + D + "<br>" + H + ":" + P + ":" + S);
	//now.getSeconds();
	$("#now").html(D + "/" + M + "/" + Y + " " + H + ":" + P + ":" + S);
}

/*
function spanWithJson() {
	$("span.json-obj").each(function () {

		var type = $(this).data('type');
		var obj = JSON.parse($(this).text());
		//$(span).text("");

		var span = "";
		$.each(obj, function (n, item) {

			if (type == "Inventory" ||
				type == "Manufactured" ||
				type == "Raw" ||
				type == "Ingredients" ||
				type == "CommodityReward" ||
				type == "Encoded"
			) {
				if (item.Name && item.Count) {
					span += "<span class='itmNameCount'>" + item.Name + "(" + item.Count + ")</span>";
				}
			}
			else if (type == "Rewards" ||
				type == "Killers" ||
				type == "Modules" ||
				type == "Items" ||
				type == "Manifest" ||
				type == "Rings" ||

				type == "Factions"
			) {
				$.each(item, function (label, str) {
					span += "<span class='label'>" + label + ": </span>";
					span += "<span class='str'>" + str + "</span><br>";
				});
				span += "<br>";
			}
			else if (type == "StarPos") {
				span += item + "<br>";
			}
			else if (type == "AtmosphereComposition" || type == "Materials") {
				span += "<span class='itmNameX100'>" + item.Name + " " + item.Percent + "%</span>";
			}
			else if (type = "System") {
				span += "<span class='itmNameStr'>" + item + "</span>";
			}
			else {
				span += JSON.stringify(item) + "<br>";
				span += "is " + typeof item + "<br>";
				span += "type " + type + "<br><br>";
			}

		});

		$(this).html(span);
	});
}*/

function HumanDate(ts, state) {

	var date = new Date($(ts).html());
	$(ts).data("date", date);

	var main = Sugar.Date.format($(ts).data().date, '{year}/{MM}/{dd} {HH}:{mm}:{ss}');
	var human = Sugar.Date.format($(ts).data().date, '{dd} {Month} {year} {HH}:{mm}:{ss}');

	$(ts).removeClass("humanDate");

	if (state == 1) {
		$(ts).html(human);
		$(ts).addClass("humanDate");
	}

	if (state == 0) {
		$(ts).html(main);
	}
}


/* ------------------------------------ */
var journal = {}
journal.v = "1.0.0";
journal.data = {};
journal.size = [];

journal.EventList = function () {

	journal.EventList.getEvent = function (ts) {
		var event = $(ts).data("event");
		location.replace("?e=" + event);
	}

	journal.EventList.click = function (ts) {
		var event = $(ts).data("event");
		var data = false;

		var obj = obj2html(journal.data[event], event);

		$("#dspLst").html(obj.html);

		$(".col-timestamp").each((a, b) => {
			//console.log(a,b);
			var myDate = Sugar.Date.format(new Date($(b).html()), '{year}/{MM}/{dd} {HH}:{mm}:{ss}');
			$(b).html(myDate);
			$(b).attr("onmouseover", "HumanDate(this,1)");
			$(b).attr("onmouseout", "HumanDate(this,0)");
		});

		var toHide = [];
		$("th.th-file").each((a, b) => {
			//console.log( a, $(b).data()  );
			toHide.push($(b).data().index);
		});

		/*$(obj.tableId).DataTable({
			"order": [[0, "desc"]],
			"columnDefs": [{
				"targets": toHide,
				"visible": false
			}],
			"language": {
				"decimal": ",",
				"thousands": "."
			}
		});*/

		$("#dspLst").fadeIn(250);

	}

	Sugar.Array.sortBy(journal.size, function (n) {
		return n.name;
	});

	var ul = "<ul>"; //"+obj.lenght+" record for: "

	$.each(journal.size, function (n, obj) {
		ul += "<li id='evt-" + v.lowerCase(obj.name) + "' title='" + obj.length + " records for: " + obj.size + "' data-size='" + obj.bytes + "' data-event='" + obj.name + "' onclick='journal.EventList.click(this)'>";
		//ul += "<li id='evt-" + v.lowerCase(obj.name) + "' title='" + obj.length + " records for: " + obj.size + "' data-size='" + obj.bytes + "' data-event='" + obj.name + "' onclick='journal.EventList.getEvent(this)'>";
		ul += Sugar.String.titleize(obj.name) + " <span>(" + obj.length + ")</span>";
		ul += "</li>";
	});

	ul += "<ul>";

	$("#listev div.ls").html(ul);
}

journal.Finder = function (ts) {

	var input = $(ts).val();
	var tg = $(ts).data().tg;
	var res = $(tg + ":icontains('" + input + "')");

	if (input.length != 0) {
		$(tg).removeClass("green");
		$(res).addClass("green");
	}
	else {
		$(tg).removeClass("green");
	}

	console.log(res, input.length);
}

journal.Run = function () {
	Msn();
	Bal();
	Cover();
	journal.EventList();
	console.log(journal);
	msn.tab(1);
}

function DT() {
	$("#dspLst table").DataTable({
		"order": [[0, "desc"]],
		/*"columnDefs": [{
			"targets": toHide,
			"visible": false
		}],*/
		"language": {
			"decimal": ",",
			"thousands": "."
		}
	});

	$("#dspLst").fadeIn(250);

}

$(document).ready(function () {

	// NEW selector
	jQuery.expr[':'].contains = function (a, i, m) {
		return jQuery(a).text().toUpperCase()
			.indexOf(m[3].toUpperCase()) >= 0;
	};

	// OVERWRITES old selecor
	jQuery.expr[':'].icontains = function (a, i, m) {
		return jQuery(a).text().toUpperCase()
			.indexOf(m[3].toUpperCase()) >= 0;
	};

	$.ajax({
		url: "db/jdb.index.json",
		success: function (s) {
			_success_ = s;
		},
		error: function (e) {
			console.log("err load db/jdb.index.json", e);
			_err_ = e;
		},
		complete: function (c) {
			_complete_ = c;
			journal.events = c.responseJSON.by_event;
			journal.time = c.responseJSON.by_time;
			journal.for_list = c.responseJSON.for_list;
			journal.cc = 0;
			_complete_.responseJSON.by_event.forEach(function (event, n) {
				$.ajax({
					url: "db/jdb." + event + ".json",
					error: function (ee) {
						console.log("err load db/jdb." + event + ".json", ee);
						_err_ = ee;
					},
					success: function (ss) {
						_success_ = ss;
					},
					complete: function (cc) {

						console.log(cc);

						var size = {
							"name": event,
							"bytes": memorySizeOf(cc.responseJSON).bytes,
							"length": cc.responseJSON.length,
							"size": memorySizeOf(cc.responseJSON).human
						}

						journal.data[event] = cc.responseJSON;

						journal.size.push(size);
						journal.cc++;
					},
				});
			});
		}
	});

	$(document).ajaxStop(function (a, b) {

		$.each(journal.size, (a, b) => {

			/*var max = 4 * 100000;

			if (b.bytes > max) {

				var arr = journal.data[b.name];

				var size = b.bytes;
				var items = b.length;

				var iks = max * items / size;

				Sugar.Array.sortBy(arr, function (n) {
					return n.timestamp;
				}, true);

				arr = arr.slice(0, Math.ceil(iks));

				journal.data[b.name] = arr;

				journal.size[a].size = memorySizeOf(arr).human;
				journal.size[a].bytes = memorySizeOf(arr).bytes;
				journal.size[a].length = arr.length;
				//console.log("::", journal.data[b.name], journal.size[a], );
			}*/
		});

		Now();

		journal.Run();
		journal.ajaxStop = a;
		Sugar.Array.sortBy(journal.size, function (n) {
			return n.bytes;
		});

		//DT();

	});

}) 
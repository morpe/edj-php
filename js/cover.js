
cover = {};
cover.comander = {};
cover.Render = function(){

	var c = this;
	this.now = Sugar.Array.last( this.journal );
	this.now.date = new Date( this.now.timestamp );
	this.html = $("#cover").html();

	this.html = replaceAll( this.html, "{cmdr}", span(this.now.Commander) );
	this.html = replaceAll( this.html, "{ship}", span(this.now.ShipName +" ["+this.now.Ship+"]") );
	this.html = replaceAll( this.html, "{stardate}", span( simpleDate(this.now.timestamp) ) );
	this.html = replaceAll( this.html, "{bal-today}", span( formatNumber(this.now.Credits) ) );

	this.html = replaceAll( this.html, "{bal-yesterday-dif}", span( formatNumber(this.balance.yersterday.dif) ) );
	this.html = replaceAll( this.html, "{bal-thisweek-dif}", span( formatNumber(this.balance.thisWeek.dif) ) );
	this.html = replaceAll( this.html, "{bal-thismonth-dif}", span( formatNumber(this.balance.thisMonth.dif) ) );

	this.html = replaceAll( this.html, "{ms-acc} ", span( this.mission.data.MissionAccepted.length ) );
	this.html = replaceAll( this.html, "{ms-comp}", span( this.mission.data.MissionCompleted.length ) );
	this.html = replaceAll( this.html, "{ms-delta}", span( this.mission.data.delta ) );
	this.html = replaceAll( this.html, "{ms-abb}", span( this.mission.data.MissionAbandoned.length ) );
	this.html = replaceAll( this.html, "{ms-fail}", span( this.mission.data.MissionFailed.length ) );
	this.html = replaceAll( this.html, "{ms-active-list}", span( this.mission.actHtml) );

	$("#cover").html(this.html);
	$("#cover").fadeIn(250);
	console.log("render!:", cover );
	
}

function Cover(){
	cover.journal = journal.data.LoadGame;
	cover.mission = msn;
	cover.balance = bal;
	cover.Render();
}
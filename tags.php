<?php

// {head}
function head()
{
	global $edj;

	foreach ($edj->css as $n => $url) {
		$head[] = "<link class='reload' rel='stylesheet' href='$url?r=".rand(1000, 9999)."'>";
	}

	foreach ($edj->js as $n => $url) {
		if (!stristr( $url, "http" )) {
			$url = "js/$url";
		}
		$head[] = "<script src='$url?r=".rand(1000, 9999)."'></script>";
	}
	
	//var_dump($head);

	return implode("\n", $head);
}


function commander()
{

	global $edj;
	$db = file_get_contents( $edj->db."jdb.LoadGame.json" );
	$db = json_decode( $db );

	$db = array_reverse( $db );

	return $db[0]->Commander;
}

function ship()
{
	
		global $edj;
		$db = file_get_contents( $edj->db."jdb.LoadGame.json" );
		$db = json_decode( $db );
	
		$db = array_reverse( $db );
	
		return $db[0]->ShipName ." <span class='type'>[". $db[0]->Ship."]</span>";
}

function stardate()
{
	
		global $edj;
		$db = file_get_contents( $edj->db."jdb.LoadGame.json" );
		$db = json_decode( $db );
	
		$db = array_reverse( $db );
	
		return dt( "d M Y H:i:s", $db[0]->timestamp);
}


function eventList()
{
	global $edj;

	$index = file_get_contents( $edj->db."jdb.index.json" );
	$index = json_decode( $index );
	$events = (array) $index->for_list;

	ksort($events);

	$ul[] = "<ul>";
	foreach ($events as $event => $cc) {
		$e = strtolower($event);
		$ul[] = "<li id='e_$event' class='event' data-event='$e' data-count='$cc' style='display:none;' onclick=\"location.replace('?table=$e') \">$event <span>($cc)<span></li>";
	}
	$ul[] = "</ul>";

	return implode("\n", $ul);
}

function eventTable()
{
	global $edj;

	if (!file_exists(  $edj->db."jdb.".$edj->get->table.".json" )) {
		$div = "<div class='s404'>file not found:<span> ".$edj->db."jdb.".$edj->get->table.".json<span></div>";
		
		return $div;
	}


	$json = file_get_contents( $edj->db."jdb.".$edj->get->table.".json" );
	$arr = json_decode( $json );
	//var_dump( $obj );

	$thead = array();
	foreach ($arr as $n => $obj) {
		foreach ($obj as $key => $val) {
			array_push( $thead, $key );
		}
	}
	$thead = array_count_values($thead);

	$table[] = "<h2>EVENT: ".$edj->get->table."</h2>";

	$table[] = "<table id='ev-".$edj->get->table."' class='table'>";

	// --------------------- table head
	$table[] = "<thead>";
	$table[] = "<tr>";
	foreach ($thead as $key => $cc) {
		$table[] = "<td class='col_$key head'>$key</td>";
	}
	$table[] = "</tr>";
	$table[] = "</thead>";

	// --------------------- table body
	$table[] = "<tbody>";
	$arr = array_reverse($arr);

	foreach ($arr as $tr => $obj) {
		$oddEven = "even";
		if ($tr % 2 == 0) {
			$oddEven = "odd";
		}

		$table[] = "<tr id='r$tr' class='$oddEven'>";
		foreach ($thead as $key => $count) {
			//var_dump( array_key_exists( $key, (array) $obj ), $val );
			$col = "col_$key";
			$id = "e$tr$key";

			$val = (array) $arr[$tr];
			
			if (isset( $val[ $key ] )) {
				$val = $val[ $key ];
			}
		
			if (array_key_exists( $key, (array) $obj )) {
				$asJson = json_encode($val);
	
				if (is_string($val)) {
					$ts = strtotime( $val );
	
					if ($ts) {
						$human = date("j M Y H:i:s", $ts);
						$order = date("Y/m/d H:i:s", $ts);
	
						$table[] = "<td id='$id' class='$col full swdate ' data-json='$asJson' data-ts='$ts' data-order='$order' data-human='$human' >$order</td>";
					} else {
						$table[] = "<td id='$id' class='$col full str ' data-json='$asJson'>$val</td>";
					}
				} elseif (is_object($val)) {
					$table[] = "<td id='$id' class='$col full obj' data-json='$asJson'>[object]</td>";
				} elseif (is_array($val)) {
					if (count($val) != 0) {
						$table[] = "<td id='$id' class='$col full arr' data-json='$asJson' >array (".count($val).")</td>";
					} else {
						$table[] = "<td id='$id' class='$col full empty' data-json='false' ></td>";
					}
				} elseif (is_bool($val)) {
					$table[] = "<td id='$id' class='$col full bool' data-json='$asJson'>$val</td>";
				} elseif (is_numeric($val)) {
					$table[] = "<td id='$id' class='$col full numeric' data-json='$asJson'>$val</td>";
				} else {
					$table[] = "<td id='$id' class='$col full unknow' data-json='$asJson' >Unknow Data</td>";
				}
			} else {
				$table[] = "<td id='$id' class='$col empty' data-json='false' ><!-- empty --></td>";
			}
		}


		$table[] = "</tr>";
	}

	$table[] = "</tbody>";
	
	// --------------------- table footer
	$table[] = "<tfoot>";
	$table[] = "<tr>";
	foreach ($thead as $key => $cc) {
		$table[] = "<td class='col_$key footer'>$key</td>";
	}
	$table[] = "</tr>";
	$table[] = "</tfoot>";

	$table[] = "</table>";
	$table[] = "<script>  ";
	$table[] = "</script>";

	return implode( "\n", $table );
}

function ajax()
{
	global $edj;

	$html[] = "<script>";
	$html[] = "server=".json_encode($edj->server).";";
	$html[] = "get=".json_encode($edj->get).";";
	$html[] = "post=".json_encode($edj->post).";";
	$html[] = "_complete_=false;";
	$html[] = "_err_=false;";
	$html[] = "db_index=".json_encode($edj->index).";";
	
	//lg( array( count( $edj->index->by_event ), count($edj->db_dir)  ) );
	$dir = count($edj->db_dir) != count( $edj->index->by_event ) + 4;
	
	$html[] = "z=".json_encode( array( $edj->db_dir, $edj->index->by_event ) ).";";


	$dbSync = dbSync();
	if ( $dbSync[2] ) {
		$html[] ="count=".json_encode( $dbSync ).";";
	} else {
		$html[] = file_get_contents("js/call_db.js");
		$html[] ="count=".json_encode( $dbSync  ).";";
	}

	if (isset( $_GET['QUERY_STRING'] )) {
		$qa = explode("=", $edj->server->QUERY_STRING);
	
		$html[] = "$.ajax({
			url:'db/jdb.".$qa[1].".json',
			success: function(s){ console.log('success',s) },
			complete: function(c){ console.log('complete',c); _complete_ = c.responseJSON; },
			error: function(c){ console.log('complete',e); _err_ = c; }
		})";
	}

	$html[] = "edj=".json_encode($edj).";";

	$html[] = "</script>";
	return implode("\n", $html);
}


function location()
{
	$db = getJdb("Location");

	$str = array();
	$dt = dt( "M d Y @ H:m:s", $db[0]->timestamp );

	if ($db[0]->Docked) {
		$str[] = "<div class='port'>";
		$str[] = "Docking at {port} on system {system} [ {alliance}, {security} ]";
		$str[] = "<br>in date: ".spanIt($dt);
		$str[] = "</div>";
	} else {
		$str[] = "<div class='position'>";
		$str[] = "Landed to {body} at {long} / {lat} on system {system} [ {alliance}, {security} ]";
		$str[] = "<br>in date: ".spanIt($dt);
		$str[] = "</div>";
	}

	$str = implode("\n", $str);
	//var_dump($db);

	$db = $db[0];

	//$port = $db->StationName;

	$str = str_ireplace( "{port}", spanIt(@$db->StationName), $str );
	//$str = str_ireplace( "{dock}",spanIt("dock"), $str );
	$str = str_ireplace( "{system}", spanIt(@$db->StarSystem), $str );
	$str = str_ireplace( "{alliance}", spanIt(@$db->SystemAllegiance), $str );
	$str = str_ireplace( "{security}", spanIt(@$db->SystemSecurity_Localised), $str );
	$str = str_ireplace( "{body}", spanIt(@$db->Body), $str );
	$str = str_ireplace( "{long}", spanIt(@$db->Longitude), $str );
	$str = str_ireplace( "{lat}", spanIt(@$db->Latitude), $str );

	return $str;
}

function mission(){
	include "mission.php";
	$obj = get_mission();
	$out = (object) array();
	

	//summary
	$ul[] = "<div class='help'>Summary show only active missions*</div>";
	$ul[] = "<ul class='summary'>"; 
	//var_dump($obj);
	foreach( $obj->destinations as $key => $arr ){
		
		
		$ul[] = "<li class='head'>$key</li>";
		$ul[] = "<ul>"; 

		if( $arr != false ){

			foreach( $arr as $label => $count ){
				$label = str_ireplace("_"," ",$label);
				$label = strtolower( $label );
				$ul[] = "<li class='item'>$label ".spanIt( "[$count]","count" )."</li>";
			}
		}

		$ul[] = "</ul>"; 
		
	}
	$ul[] = "</ul>";

	$out->summary = implode("\n",$ul);

	//lg($obj);

	return $out;

}
<?php
date_default_timezone_set("Europe/Rome");
error_reporting(E_ALL);
ini_set('memory_limit', '200M');
ini_set('max_execution_time', 300);
include "edj.php";
include "functions.php";
include "tags.php";


function bigdata_by_events($arr)
{

	$tmp = (object)array();
	foreach ($arr as $n => $obj) {
		$e = strtolower($obj->event);
		$tmp->$e = array();
	}

	foreach ($arr as $n => $obj) {
		$e = strtolower($obj->event);
		array_push($tmp->$e, $obj);
	}

	return $tmp;
}

function write_jdb($arr)
{
	global $edj;

	$tmp_data = array();

	$req = strtolower($edj->get->f);

	foreach ($arr as $n => $obj) {

		$event = strtolower($obj->event);

		if ($req == $event) {
			array_push($tmp_data, $obj);
		}
	}

	if (count($tmp_data) != 0) {
		file_put_contents($edj->db . "jdb.$req.json", json_encode($tmp_data));
		echo json_encode(array($edj->get));
	}
}

$edj->config = config();

if (isset($edj->get->f)) {
	$edj->init = init();
	$BigData = getBigData();
	$edj->index = dbIndex();
	$edj->db_dir = scandir($edj->db);

	write_jdb($BigData);
}

if (isset($edj->get->r)) {

	$log_list = array();
	foreach (scandir($edj->config->edj) as $n => $f) {

		if (stristr($f, "journal.")) {
			array_push($log_list, $f);
		}

	}

	if (!if_dir_sync()) {
		$edj->init = init();
		$BigData = getBigData();
		//********************

		//********************

		$edj->init = true;
		$edj->index = dbIndex();
	}

	if ($edj->get->r == "log") {
		$edj->init = init();
		$BigData = getBigData();
		$events = bigdata_by_events($BigData);
		foreach (scandir($edj->db) as $n => $file) {

			if (stristr($file, "jdb.")) {

				$json = file_get_contents($edj->db . $file, FILE_USE_INCLUDE_PATH);
				$e = str_ireplace("jdb.", "", $file);
				$e = str_ireplace(".json", "", $e);

				if (isset($events->$e)) {

					$a = md5($json);
					$b = md5(json_encode($events->$e));
					if ($a != $b) {
						file_put_contents($edj->db . $file, json_encode($events->$e), FILE_USE_INCLUDE_PATH);
					}

				}
				else {
					var_dump("$file not fount");
				}
			}

			$index = file_get_contents( $edj->db."jdb.index.json" );
			$index = json_decode( $index );
			foreach( $index->log_md5 as $a => $b ){
				//$md5 = md5( file_get_contents( $edj->config->edj.$log ) );
	
				$log = key($b);
				$md5 = $b->$log;
				$journal =  md5( file_get_contents( $edj->config->edj.$log ) );
	
				if( $md5 != $journal ){

					array_push( $log_list, $log );
					
					$cp = copy( $edj->config->edj.$log, $edj->tmp->journal."/$log" );
	
					if( $cp ){
						
						$i= dbIndex();
					}

					var_dump( array( $log ) );
				}
			}
		}
	}

	if ($edj->get->r == "true") {
		$edj->init = init();
		$BigData = getBigData();
		$events = bigdata_by_events($BigData);
		foreach (scandir($edj->db) as $n => $file) {

			if (stristr($file, "jdb.")) {

				$json = file_get_contents($edj->db . $file, FILE_USE_INCLUDE_PATH);
				$e = str_ireplace("jdb.", "", $file);
				$e = str_ireplace(".json", "", $e);

				if (isset($events->$e)) {

					$a = md5($json);
					$b = md5(json_encode($events->$e));
					if ($a != $b) {
						file_put_contents($edj->db . $file, json_encode($events->$e), FILE_USE_INCLUDE_PATH);
					}

				}
				else {
					//lg("$file not fount");
				}
			}
		}

		$index = file_get_contents( $edj->db."jdb.index.json" );
		$index = json_decode( $index );
		foreach( $index->log_md5 as $a => $b ){
			//$md5 = md5( file_get_contents( $edj->config->edj.$log ) );
	
			$log = key($b);
			$md5 = $b->$log;
			$journal =  md5( file_get_contents( $edj->config->edj.$log ) );
	
			if( $md5 != $journal ){
	
				array_push( $log_list, $log.".old" );
				
				$cp = copy( $edj->config->edj.$log, $edj->tmp->journal."/$log" );
	
				if( $cp ){
					
					$i= dbIndex();
				}
			}
		}

	}


	header('Content-type: text/html; charset=UTF-8');
	echo json_encode($log_list);
}

if (isset($edj->get->log)) {

	$data = array();


	$arr = file($edj->config->edj . "/" . $edj->get->log);
	foreach ($arr as $n => $json) {
		array_push($data, json_decode($json));
	}

	foreach (scandir($edj->tmp->journal) as $n => $file) {

		if (stristr($file, "journal.")) {

			$arr = file($edj->tmp->journal . $file);
			foreach ($arr as $n => $json) {
				array_push($data, json_decode($json));
			}
		}

	}


	include "mission.php";
	$data = parse_mission($data);

	header('Content-type: text/html; charset=UTF-8');
	echo json_encode($data);
}


?>